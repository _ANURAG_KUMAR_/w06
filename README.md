# W06 Chat Example

A simple chat demo using node.js, express, and socket.io

## Requirements

- Install Git version control system
- Install Node.js, an open-source, cross-platform JavaScript run-time environment for executing JavaScript code on the server. 
- Install Visual Studio Code for editing. 


## Run it

Open a command window in your w06 folder.

Run **npm install** to install all the dependencies in the package.json file.

Run **node server.js** or **node server** to start the server.  (Hit CTRL-C to stop.)

```Bash
> npm install
> node server
```

Point your browser to `http://localhost:8081`.

## References

- http://javabeginnerstutorial.com/javascript-2/create-simple-chat-application-using-node-js-express-js-socket-io/

- http://socket.io/get-started/chat/

- https://github.com/socketio/socket.io

- http://socket.io/

## Chat with Friends

Open commnand window, type **ipconfig**. 

```PowerShell
ipconfig
```

